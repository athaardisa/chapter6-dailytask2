const router = require('express').Router()
// Controller
const Product = require('./products')
const Warehouse = require('./warehouses')
const Auth = require('./auth')

// API products
router.use('/api/v1/', Product)

// API warehouse
router.use('/api/v1/', Warehouse)

// API auth
router.use('/api/v1/auth/', Auth)

module.exports = router
