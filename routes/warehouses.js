const router = require('express').Router()

const Warehouse = require('../controller/warehouseController')

router.get('/warehouses', Warehouse.findWarehouses)
router.post('/warehouses', Warehouse.createWarehouse)
router.get('/warehouses/:id', Warehouse.findWarehouseById)

module.exports = router